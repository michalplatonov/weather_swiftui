//
//  CityChoosingViewModel.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

import Combine
import SwiftUI

class CityChoosingViewModel:ObservableObject{
    @Published var weatherDetail:WeatherDetail?
    @Published var cityImage:CityImage?
    @Published var searchText = String()
    private var anyCancellable = Set<AnyCancellable>()
    init(){
        $searchText.debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .removeDuplicates()
            .sink { [weak self] text in
                guard text.isValidCityName() == true else {
                    self?.weatherDetail = nil
                    self?.cityImage = nil
                    return
                }
                self?.getCity(searchText: text)
            }
            .store(in: &anyCancellable)
    }
    func getCity(searchText:String){
        let queryItem = URLQueryItem(name: "q", value: searchText)
        let endPoint = OpenWeatherEndpoint(path: .weather, queryItems: [queryItem])
        let weatherDetailService = ApiRequestService<WeatherDetail>(endPoint: endPoint)
        weatherDetailService.getApiDetails(urlSession: .urlSession)
            .sink { result in
                switch result{
                case .finished:
                    break
                case .failure(_):
                    self.weatherDetail = nil
                    self.cityImage = nil
                }
            } receiveValue: { weatherDetail in
                self.weatherDetail = weatherDetail
                self.getImage(searchText: searchText)
            }
            .store(in: &anyCancellable)
    }
    func getImage(searchText:String){
        let imageEnpoint = ImageEndPoint(path: .imageSearch, queryItems: [URLQueryItem(name: "q", value: searchText)])
        let imageService = ApiRequestService<CityImage>(endPoint: imageEnpoint)
        imageService.getApiDetails(urlSession: .imageUrlSession)
            .sink { result in
                switch result{
                case .finished:
                    break
                case .failure(_):
                    self.cityImage = nil
                }
            } receiveValue: { cityImage in
                self.cityImage = cityImage
            }
            .store(in: &anyCancellable)
    }
    
}
