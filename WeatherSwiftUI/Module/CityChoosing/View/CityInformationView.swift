//
//  CityInformationView.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI
struct CityInformationView: View {
    @ObservedObject var viewModel:CityChoosingViewModel
    var body: some View {
        VStack{
            Text(viewModel.weatherDetail != nil ? "\(viewModel.weatherDetail?.name ?? ""), \(viewModel.weatherDetail?.sys.country ?? "")" : "No city found")
                .font(viewModel.weatherDetail != nil ? .largeTitle : .title3)
            HStack(spacing:0){
                WebImage(url: URL(string: viewModel.weatherDetail?.weather.first?.icon ?? ""))
                    .resizable()
                    .frame(width: 50, height: 50, alignment: .center)
                    .scaledToFit()
                Text("\(viewModel.weatherDetail?.main.temp ?? "0")°")
                    .font(.largeTitle)
            }
            .opacity(viewModel.weatherDetail != nil ? 1 : 0)
            NavigationLink(destination: CurrentForecastView(weatherDetail: viewModel.weatherDetail)) {
                Text("Show Forecast")
                    .frame(width: 250, height: 40)
                    .background(Color.secondary.opacity(0.7))
                    .foregroundColor(Color.white)
            }
            .cornerRadius(10)
            .opacity(viewModel.weatherDetail != nil ? 1 : 0)
        }
        
    }
}

