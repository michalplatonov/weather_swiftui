//
//  ContentView.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI
struct CityChoosing: View {
    @StateObject private var viewModel = CityChoosingViewModel()
    
    var body: some View {
        NavigationView{
                VStack{
                    SearchBar(text: $viewModel.searchText, placeholder: "Search city...")
                    Spacer()
                    CityInformationView(viewModel: viewModel)
                        .animation(.interactiveSpring())
                    Spacer()
                }
                .background(
                    WebImage(url: URL(string: viewModel.cityImage?.value.first?.contentURL ?? ""))
                        .resizable()
                        .scaledToFill()
                        .blur(radius: 30)
                        .opacity(viewModel.weatherDetail != nil ? 1 : 0)
                )
            .navigationTitle("City Forecast")
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct CityChoosing_Previews: PreviewProvider {
    static var previews: some View {
        CityChoosing()
    }
}
