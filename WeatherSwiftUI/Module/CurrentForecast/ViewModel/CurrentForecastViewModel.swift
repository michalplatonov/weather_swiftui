//
//  CurrentWeatherViewModel.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Combine
import Foundation

class CurrentForecastViewModel:ObservableObject{
    @Published var currentForecast:CurrentForecastData?
    private var anyCancellable = Set<AnyCancellable>()
    
    func getCurrentForecast(weatherDetail:WeatherDetail?){
        guard let weatherDetail = weatherDetail else {
            return
        }
        let lat = weatherDetail.coord.lat
        let lon = weatherDetail.coord.lon
        let latQuery = URLQueryItem(name: "lat", value: "\(lat)")
        let lonQuery = URLQueryItem(name: "lon", value: "\(lon)")
        let excludeQuery = URLQueryItem(name: "exclude", value: "minutely")
        let endPoint = OpenWeatherEndpoint(path: .oneCall, queryItems: [latQuery,lonQuery,excludeQuery])
        let currentForecastService = ApiRequestService<CurrentForecastData>(endPoint: endPoint)
        currentForecastService.getApiDetails(urlSession: .urlSession)
            .sink { result in
                switch result{
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            } receiveValue: { currentForecast in
                self.currentForecast = currentForecast
            }
            .store(in: &anyCancellable)
    }
    
}
