//
//  HourlyScrollView.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct HourlyScrollView: View {
    @ObservedObject var viewModel:CurrentForecastViewModel
    var body: some View {
        ScrollView(.horizontal){
            LazyHStack{
                if viewModel.currentForecast?.current != nil{
                    ForEach(0..<viewModel.currentForecast!.hourly.count,id:\.self){id in
                        HourlyView(hourly: viewModel.currentForecast?.hourly[id])
                    }
                }
            }
            .padding(.leading,10)
            .padding(.trailing,10)
        }
        .frame(height: 100, alignment: .center)
        .background(Color.gray.opacity(0.4))
        .clipShape(RoundedRectangle(cornerRadius: 10.0, style: .continuous))
        .padding(.leading,10)
        .padding(.trailing,10)
        .opacity(viewModel.currentForecast != nil ? 1 : 0)
    }
}


