//
//  DailyScrollView.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct DailyScrollView: View {
    @ObservedObject var viewModel:CurrentForecastViewModel
    var body: some View {
        ScrollView(.vertical){
            LazyVStack{
                if viewModel.currentForecast?.current != nil{
                    ForEach(0..<viewModel.currentForecast!.daily.count,id:\.self){id in
                        DailyView(daily: viewModel.currentForecast?.daily[id])
                    }
                }
            }
        }
        .background(Color.gray.opacity(0.4))
        .clipShape(RoundedRectangle(cornerRadius: 10.0, style: .continuous))
        .padding(.leading,10)
        .padding(.trailing,10)
        .opacity(viewModel.currentForecast != nil ? 1 : 0)
    }
}

