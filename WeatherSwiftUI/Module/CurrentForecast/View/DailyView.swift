//
//  ВфшднМшуц.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct DailyView: View {
    var daily:DailyElement?
    var body: some View {
        HStack(){
            Text(String(daily?.dt.timestampToString(with: .ddMMMM) ?? ""))
            Spacer()
                .frame(width: 50)
            WebImage(url: URL(string: daily?.weather.first?.icon ?? ""))
                .resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .scaledToFit()
            Spacer()
            Text("\(daily?.temp.day ?? "")°")
                .frame(alignment: .leading)
        }
        .padding(.leading,10)
        .padding(.trailing,10)
        
    }
}
