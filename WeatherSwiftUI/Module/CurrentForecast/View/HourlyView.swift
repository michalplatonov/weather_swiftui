//
//  HourlyView.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct HourlyView: View {
    var hourly:Current?
    var body: some View {
        VStack(spacing:5){
            Text(hourly?.dt.timestampToString(with: .hhMM) ?? "")
                .font(.system(size: 16))
            WebImage(url: URL(string: hourly?.weather.first?.icon ?? ""))
                .resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .scaledToFit()
            Text("\(hourly?.temp ?? "")°")
        }
    }
}
