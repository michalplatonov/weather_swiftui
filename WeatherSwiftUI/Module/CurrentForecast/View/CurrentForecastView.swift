//
//  CurrentWeatherView.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//
import SwiftUI
import SDWebImageSwiftUI
struct CurrentForecastView: View {
    var weatherDetail:WeatherDetail?
    @StateObject private var viewModel = CurrentForecastViewModel()
    
    var body: some View {
        VStack(spacing:10){
            VStack{
                Text(weatherDetail?.name ?? "")
                    .font(.system(size: 24))
                Text(weatherDetail?.weather.first?.main ?? "")
                    .font(.system(size: 14))
                Text("\(weatherDetail?.main.temp ?? "")°")
                    .font(.system(size: 36))
            }
            .onAppear(){
                viewModel.getCurrentForecast(weatherDetail: weatherDetail)
            }
            HourlyScrollView(viewModel: viewModel)
            DailyScrollView(viewModel: viewModel)
            Spacer()
        }
        
    }
}





