//
//  String+REgex.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation
extension String{
    func isValidCityName() -> Bool{
        let cityRegEx = "^[a-zA-Z'\\sÀ-ž,.-]{2,25}$"
        let cityPred = NSPredicate(format: "SELF MATCHES %@", cityRegEx)
        return cityPred.evaluate(with: self)
    }
}
