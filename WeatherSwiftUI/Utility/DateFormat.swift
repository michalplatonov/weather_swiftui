//
//  DateFormat.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import Foundation


enum DateFormat:String{
    case ddMMMM = "dd MMMM"
    case hhMM = "HH:mm"
}
