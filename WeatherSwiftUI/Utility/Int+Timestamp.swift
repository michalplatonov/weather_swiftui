//
//  Int+Timestamp.swift
//  WeatherSwiftUI
//
//  Created by Mykhailo Platonov on 13/12/2021.
//

import Foundation


extension Int{
    func timestampToString(with format:DateFormat)->String{
        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        let timezoneEpochOffset = (self + timezoneOffset)
        let timeZoneOffsetDate = Date(timeIntervalSince1970: TimeInterval(timezoneEpochOffset))
        let locale = Locale(identifier: "pl")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.locale = locale
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 1)
        let dateStr = dateFormatter.string(from: timeZoneOffsetDate)
        return dateStr
    }
}
