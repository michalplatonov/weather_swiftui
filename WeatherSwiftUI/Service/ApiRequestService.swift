//
//  WeatherDetailsService.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation
import Combine

class ApiRequestService<T:Codable>{
    var endPoint:Endpoint
    init(endPoint:Endpoint) {
        self.endPoint = endPoint
    }
    func getApiDetails(urlSession:URLSession) -> AnyPublisher<T,Error>{
        let decoder = JSONDecoder()
        guard let url = endPoint.url else {
            return Fail(error: NetworkingError.invalidURL).eraseToAnyPublisher()
        }
        return urlSession.dataTaskPublisher(for: url)
            .mapError{ error in
                return NetworkingError.noInternet
            }
            .tryMap{ result in
                guard let response = result.response as? HTTPURLResponse, 200..<299 ~= response.statusCode else {
                    let error = try decoder.decode(ApiError.self, from: result.data)
                    throw error
                }
                return try decoder.decode(T.self, from: result.data)
            }
            .receive(on: DispatchQueue.main)
            .subscribe(on: DispatchQueue.global())
            .eraseToAnyPublisher()
    }
}
