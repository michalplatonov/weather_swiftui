//
//  EndPoint.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation


struct OpenWeatherEndpoint:Endpoint{
    let path: String
    var queryItems: [URLQueryItem]?
    private let apiKey = "00e802e4403be461e62a13dcd8bc447c"
    enum Path:String{
        case weather = "weather"
        case oneCall = "onecall"
    }
    init(path: Path, queryItems: [URLQueryItem] = [URLQueryItem]()) {
        self.path = path.rawValue
        self.queryItems = queryItems
        let appId = URLQueryItem(name: "appid", value: apiKey)
        let units = URLQueryItem(name: "units", value: "metric")
        self.queryItems?.append(contentsOf: [appId,units])
    }
    var url:URL?{
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.openweathermap.org"
        components.path = "/data/2.5/" + path
        components.queryItems = queryItems
        return components.url
    }
    
    
}

