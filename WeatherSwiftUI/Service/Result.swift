//
//  Result.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

enum NetworkingError: Error {
    case invalidURL
    case badResponse
    case noInternet
    case unexpectedStatus(ApiError)
}

struct ApiError:Error,Codable {
    var code:String
    var message:String? = nil
    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case message
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let codeInt = try? container.decode(Int.self, forKey: .code)
        let codeString = try? container.decode(String.self, forKey: .code)
        let message = try? container.decode(String.self, forKey: .message)
        self.code = codeString ?? String(codeInt ?? 0)
        self.message = message
    }
}
