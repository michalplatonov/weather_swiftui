//
//  ImageEnpoint.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

struct ImageEndPoint:Endpoint{
    enum Path:String{
        case imageSearch = "/images/search"
    }
    let path: String
    var queryItems: [URLQueryItem]?
    init(path: Path, queryItems: [URLQueryItem] = [URLQueryItem]()) {
        self.path = path.rawValue
        self.queryItems = queryItems
        let count = URLQueryItem(name: "count", value: "1")
        self.queryItems?.append(count)
    }
    var url:URL?{
        var components = URLComponents()
        components.scheme = "https"
        components.host = "bing-image-search1.p.rapidapi.com"
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
    
    
}
