//
//  UrlSession.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

extension URLSession{

    static let urlSession: URLSession = {
        let urlSessionConfig = URLSessionConfiguration.default
        urlSessionConfig.timeoutIntervalForRequest = 30
        let session = URLSession(configuration: urlSessionConfig)
        return session
    }()
    
    static let imageUrlSession: URLSession = {
        let headers = [
            "x-rapidapi-host": "bing-image-search1.p.rapidapi.com",
            "x-rapidapi-key": "84966b10demsh80b73a74da9f9a3p14cb14jsn52f887e8521a"
        ]
        let urlSessionConfig = URLSessionConfiguration.default
        urlSessionConfig.timeoutIntervalForRequest = 10
        urlSessionConfig.httpAdditionalHeaders = headers
        let session = URLSession(configuration: urlSessionConfig)
        return session
    }()
    
}

