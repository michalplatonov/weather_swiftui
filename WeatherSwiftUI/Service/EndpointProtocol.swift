//
//  EndpointProtocol.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

protocol Endpoint{
    var path:String { get}
    var queryItems:[URLQueryItem]? { get }
    var url:URL? { get }
}
