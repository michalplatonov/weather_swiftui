//
//  Weather.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

struct Weather: Codable {
    let main: String
    let icon: String
    
    enum CodingKeys: String, CodingKey {
        case main, icon
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let main = try container.decode(String.self, forKey: .main)
        let icon = try container.decode(String.self, forKey: .icon)
        self.main = main
        self.icon = "http://openweathermap.org/img/wn/\(icon)@2x.png"
    }
}
