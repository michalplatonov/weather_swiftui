//
//  CurrentForecast.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

struct CurrentForecastData: Codable {
    let lat, lon: Double
    let current: Current
    let hourly: [Current]
    let daily:[DailyElement]
}
struct DailyElement: Codable {
    let dt:Int
    let temp: Temp
    let weather: [Weather]
    enum CodingKeys: String, CodingKey {
        case temp, weather,dt
    }
}
struct Current: Codable {
    let dt:Int
    let temp, feelsLike: String
    let weather: [Weather]
    enum CodingKeys: String, CodingKey {
        case temp, weather,dt
        case feelsLike = "feels_like"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dt = try container.decode(Int.self, forKey: .dt)
        let temp = try? container.decode(Double.self, forKey: .temp)
        let feelsLike = try? container.decode(Double.self, forKey: .feelsLike)
        let weather = try container.decode([Weather].self, forKey: .weather)
        self.dt = dt
        self.temp = String(format: "%.0f", temp ?? 0.0)
        self.feelsLike = String(format: "%.0f", feelsLike ?? 0.0)
        self.weather = weather
    }
}


struct Temp: Codable {
    let day, night: String
    enum CodingKeys: String, CodingKey {
        case day, night
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let day = try container.decode(Double.self, forKey: .day)
        let night = try container.decode(Double.self, forKey: .night)
        self.day = String(format: "%.0f", day)
        self.night = String(format: "%.0f", night)
    }
}

