//
//  CityImage.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation

struct CityImage: Codable {
    let value: [Value]
}

// MARK: - Value
struct Value: Codable {
    let contentURL: String
    enum CodingKeys: String, CodingKey {
        case contentURL = "contentUrl"
    }
}
