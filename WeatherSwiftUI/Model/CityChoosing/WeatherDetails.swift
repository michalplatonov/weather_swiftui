//
//  WeatherDetails.swift
//  WeatherSwiftUI
//
//  Created by Михаил Платонов on 12/12/2021.
//

import Foundation
import SwiftUI

struct WeatherDetail: Codable {
    let coord: Coord
    let weather: [Weather]
    let main: Main
    let sys: Sys
    let name: String
}


// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double
}

// MARK: - Main
struct Main: Codable {
    let temp:String
    
    enum CodingKeys: String, CodingKey {
        case temp
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let temp = try? container.decode(Double.self, forKey: .temp)
        self.temp = String(format: "%.0f", temp ?? 0.0)
    }
}

// MARK: - Sys
struct Sys: Codable {
    let country: String
}


